<?php

class Psr4Autoloader
{
    private $files = array();

    public function add($namespace, $path)
    {
//        if namespace doesn't exist, create an associative array
//        where the key is a namespace prefix and the value
//        is an array of base directories for classes in that namespace
        if (!isset($this->files[$namespace])) {
            $this->files[$namespace] = array();
        }

        array_push($this->files[$namespace], $path); //add the new path to the new array

        return $this;
    }

    public function loadClass($class)
    {
        $prefix = $class;

        while (false !== ($pos = strrpos($prefix, '\\'))) {
            // retain the trailing namespace separator in the prefix
            $prefix = substr($class, 0, $pos + 1);

            // the rest is the relative class name
            $relative_class = substr($class, $pos + 1);

            $mapped_file = false;
            if (isset($this->files[$prefix])) {
                // look through base directories for this namespace prefix
                foreach ($this->files[$prefix] as $base_dir) {
                    // replace the namespace prefix with the base directory,
                    // replace namespace separators with directory separators
                    // in the relative class name, append with .php
                    $file = $base_dir
                        . str_replace('\\', '/', $relative_class)
                        . '.php';

                    // if the mapped file exists, require it
                    if ($this->requireFile($file)) {
                        // yes, we're done
                        $mapped_file = $file;
                        break;
                    }
                }
            }

            if ($mapped_file) {
                return $mapped_file;
            }

            // remove the trailing namespace separator for the next iteration of strrpos()
            $prefix = rtrim($prefix, '\\');
        }
    }

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }


    private function requireFile($file)
    {
        if (file_exists($file)) {
            require $file;
            return true;
        }

        return false;
    }
}

$autoloader = new Psr4Autoloader();
$autoloader
    ->add('Nfq\\Academy\\Homework\\', __DIR__.'/src/')
//    ->add('Demo\\Demo\\', __DIR__.'/src/')
//    ->add('Acme\\', __DIR__.'/vendor/acme/')
    ->register();
